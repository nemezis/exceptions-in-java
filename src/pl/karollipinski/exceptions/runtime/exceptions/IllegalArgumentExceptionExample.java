package pl.karollipinski.exceptions.runtime.exceptions;

public class IllegalArgumentExceptionExample {

	/**
	 * @Runtime Exceptions
	 * 
	 *          Runtime exceptions extend RuntimeException. They don’t have to
	 *          be handled or declared. They can be thrown by the programmer or
	 *          by the JVM.
	 * 
	 *          IllegalArgumentException Thrown by the programmer to indicate
	 *          that a method has been passed an illegal or inappropriate
	 *          argument
	 * 
	 * @param args
	 */

	int numberEggs;

	public void setNumberEggs(int numberEggs) {
		if (numberEggs < 0)
			throw new IllegalArgumentException("# eggs must not be negative");
		this.numberEggs = numberEggs;
	}

	public static void main(String[] args) {
		new IllegalArgumentExceptionExample().setNumberEggs(-1);
	}

}
