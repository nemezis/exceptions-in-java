package pl.karollipinski.exceptions.runtime.exceptions;

public class NullPointerExceptionExample {

	/**
	 * @Runtime Exceptions
	 * 
	 *          Runtime exceptions extend RuntimeException. They don’t have to
	 *          be handled or declared. They can be thrown by the programmer or
	 *          by the JVM.
	 * 
	 *          NullPointerException Thrown by the JVM when there is a null
	 *          reference where an object is required
	 * 
	 * @param args
	 */

	String name;

	public void printLength() throws NullPointerException {
		System.out.println(name.length());
	}

	public static void main(String[] args) {
		new NullPointerExceptionExample().printLength();
	}

}
