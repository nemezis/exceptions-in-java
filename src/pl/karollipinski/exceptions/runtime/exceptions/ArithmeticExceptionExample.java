package pl.karollipinski.exceptions.runtime.exceptions;

public class ArithmeticExceptionExample {

	/**
	 * @Runtime Exceptions
	 * 
	 *          Runtime exceptions extend RuntimeException. They don’t have to
	 *          be handled or declared. They can be thrown by the programmer or
	 *          by the JVM.
	 * 
	 *          ArithmeticException Thrown by the JVM when code attempts to
	 *          divide by zero
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		int answer = 11 / 0;
	}

}
