package pl.karollipinski.exceptions.runtime.exceptions;

public class ClassCastExceptionExample {

	/**
	 * @Runtime Exceptions
	 * 
	 *          Runtime exceptions extend RuntimeException. They don’t have to
	 *          be handled or declared. They can be thrown by the programmer or
	 *          by the JVM.
	 * 
	 *          ClassCastException Thrown by the JVM when an attempt is made to
	 *          cast an exception to a subclass of which it is not an instance
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		String type = "moose";
		Object obj = type;
		Integer number = (Integer) obj;
	}

}
