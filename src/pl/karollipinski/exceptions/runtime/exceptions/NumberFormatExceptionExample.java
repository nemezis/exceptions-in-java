package pl.karollipinski.exceptions.runtime.exceptions;

public class NumberFormatExceptionExample {

	/**
	 * @Runtime Exceptions
	 * 
	 *          Runtime exceptions extend RuntimeException. They don’t have to
	 *          be handled or declared. They can be thrown by the programmer or
	 *          by the JVM.
	 * 
	 *          NumberFormatException Thrown by the programmer when an attempt
	 *          is made to convert a string to a numeric type but the string
	 *          doesn’t have an appropriate format
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Integer.parseInt("abc");
	}

}
