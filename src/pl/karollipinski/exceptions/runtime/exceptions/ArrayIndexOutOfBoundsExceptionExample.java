package pl.karollipinski.exceptions.runtime.exceptions;

public class ArrayIndexOutOfBoundsExceptionExample {

	/**
	 * @Runtime Exceptions
	 * 
	 *          Runtime exceptions extend RuntimeException. They don’t have to
	 *          be handled or declared. They can be thrown by the programmer or
	 *          by the JVM.
	 * 
	 *          ArrayIndexOutOfBoundsException Thrown by the JVM when code uses
	 *          an illegal index to access an array
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		int[] countsOfMoose = new int[3];
		System.out.println(countsOfMoose[-1]);
	}

}
