package pl.karollipinski.test;

public class Question2 {

	public void go() {
		System.out.print("A");
		try {
			stop();
		} catch (ArithmeticException e) {
			System.out.print("B");
		} finally {
			System.out.print("C");
		}
		System.out.print("D");
	}

	public void stop() {
		System.out.print("E");
		Object x = null;
		x.toString();
		System.out.print("F");
	}

	public static void main(String[] args) {
		new Question2().go();
		// A. AE
		// B. AEBCD
		// C. AEC
		// D. AECD
		// E. No output appears other than the stack trace.

	}

}
