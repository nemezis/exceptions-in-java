package pl.karollipinski.test;

public class Question3 {

	public int go(int a, int b) {
		try {

			return a / b;
		} catch (RuntimeException e) {
			return -1;
		//} catch (ArithmeticException e) {
		//	return 0;
		} finally {
			System.out.print("done");
		}
	}

	public static void main(String[] args) {

		new Question3().go(2,4);
		// A. -1
		// B. 0
		// C. done-1
		// D. done0
		// E. The code does not compile.
		// F. An uncaught exception is thrown.

	}

}
