package pl.karollipinski.test;

import java.io.IOException;

class Connection implements java.io.Closeable {

	public void close() throws IOException {
		throw new IOException("Close Exception");
	}
}

public class Question {

	public static void main(String[] args) {
		try (Connection c = new Connection()) {
			throw new RuntimeException("RuntimeException");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		// What is the result?
		// A. Close Exception
		// B. RuntimeException
		// C. RuntimeException and then CloseException
		// D. Compilation fails
		// E. The stack trace of an uncaught exception is printed
	}
}


