package pl.karollipinski.test;

import java.io.IOException;

public class Question4 {

	protected static int m1() {
		int x =1;
		try {
			throw new NullPointerException();
		} catch (NullPointerException | ArithmeticException e) {
			System.out.println("catch");
			x=2;
			return x;
		} finally {
			System.out.println("finally");
			x=6;
			return x;
		}
	}

	public static void main(String[] args) {
		System.out.println(m1());
	}
}
