package pl.karollipinski.user.exception;

class MyException2 extends Exception {
	public MyException2(String message) {
		super(message);
	}
}

public class ExceptionSample {
	public static void main(String args[]) throws Exception {
		ExceptionSample es = new ExceptionSample();
		es.displayMymsg();
	}

	public void displayMymsg() throws MyException2 {
		for (int j = 8; j > 0; j--) {
			System.out.println("j= " + j);
			if (j == 7) {
				throw new MyException2("This is my own Custom Message");
			}
		}
	}
}