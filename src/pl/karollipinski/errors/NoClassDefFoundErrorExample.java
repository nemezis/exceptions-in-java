package pl.karollipinski.errors;

public class NoClassDefFoundErrorExample {

	/**
	 * @Errors
	 * 
	 * 		Errors extend the Error class. They are thrown by the JVM and
	 *         should not be handled or declared.
	 * 
	 *         NoClassDefFoundError Thrown by the JVM when a class that the code
	 *         uses is available at compile time but not runtime.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		// NoClassDefFoundError occurs when Java can’t fi nd the class at
		// runtime.

	}

}
