package pl.karollipinski.errors;

public class ExceptionInInitializerErrorExample {

	/**
	 * @Errors
	 * 
	 * 		Errors extend the Error class. They are thrown by the JVM and
	 *         should not be handled or declared.
	 * 
	 *         ExceptionInInitializerError Thrown by the JVM when a static
	 *         initializer throws an exception and doesn’t handle it.
	 * 
	 * @param args
	 */

	static {
		int[] countsOfMoose = new int[3];
		int num = countsOfMoose[-1];
	}

	public static void main(String[] args) {
		// Exception in thread "main" java.lang.ExceptionInInitializerError
		// Caused by: java.lang.ArrayIndexOutOfBoundsException: -1
		// at pl.karollipinski.ErrorsExample.<clinit>(ErrorsExample.java:26)

	}

}
