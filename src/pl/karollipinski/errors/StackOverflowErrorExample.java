package pl.karollipinski.errors;

public class StackOverflowErrorExample {

	/**
	 * @Errors
	 * 
	 * 		Errors extend the Error class. They are thrown by the JVM and
	 *         should not be handled or declared.
	 * 
	 *         StackOverflowError Thrown by the JVM when a method calls itself
	 *         too many times (this is called infi nite recursion because the
	 *         method typically calls itself without end) .
	 * 
	 * 
	 * @param args
	 */

	public static void doNotCodeThis(int num) {
		doNotCodeThis(1);
	}

	public static void main(String[] args) {
		doNotCodeThis(0);
		// Exception in thread "main" java.lang.StackOverflowError
	}

}
